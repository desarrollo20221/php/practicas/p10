<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $longitud= mt_rand(10,1000);
        
        echo "<p>EL tamaño es: $longitud</p>";               
        ?>
        <svg width="<?= $longitud ?>px" height="10px">
        <line x1="1" y1="1" x2="<?= $longitud ?>" y2="5" stroke="black" stroke-width="10">
        </svg>
    </body>
</html>
