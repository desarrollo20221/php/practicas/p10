<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <script>
            window.addEventListener("load",arranca);
            
            function arranca(){
                document.querySelector("line").addEventListener("click",salta);
            }
            function salta(e) {
                var valor;
                valor = Math.random()*(1000-1)+1;
                e.target.setAttribute("x2",valor);
                document.querySelector("p").innerHTML=`Longitud: ${valor}`;
            }
        </script>
    </head>
    <body>
        <?php
            $longitud = rand(1,100);
            
            echo "<p>Longitud: $longitud</p>";
            echo "<br>";
        ?>
        <svg width="1000px" height="10px">
        <line x1="1" y1="1" x2="<?= $longitud ?>" y2="5" stroke="red" stroke-width="10">
        </svg>
    </body>
</html>
