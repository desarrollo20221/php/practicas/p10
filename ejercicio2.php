<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // creas variable con numero aleatorio
        $longitud= mt_rand(1,100);
        
        // imprimes el numero aleatorio generado
        echo "<p>EL tamaño es: $longitud</p>";               
        ?>
        <!-- Pintas la linea -->
        <svg width="<?= $longitud ?>px" height="10px">
        <line x1="1" y1="1" x2="<?= $longitud ?>" y2="5" stroke="red" stroke-width="10">
        </svg>
    </body>
</html>
