<?php

function calculaColor() {
    $color = "rgb(" . rand(0, 225) . ", " . rand(0, 225) . ", " . rand(0, 225) . ")";
    return $color;
}

// no recomendado salvo casos de mucho HTML
function dibujaCirculo1($x, $y) {
    ?>
    <!--<circle cx="<?= $x ?>" cy="<?= $y ?>" r="50" fill="<?= calculaColor() ?>"/>;-->

    <?php
}

function dibujaCirculo($x, $y) {
    //echo "<circle cx=\"$x\" cy=\"$y\" r=\"50\" fill=\"" .calculaColor() ."\"/>";
    echo '<circle cx="' . $x . '" cy="' . $y . '" r="50" fill="' . calculaColor() . '" />';
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        
    </head>
    <body>
        <svg width="1000px" height="1000px" style="display: block;margin: 0px auto;">
        <?php
        dibujaCirculo(50, 50);
        dibujaCirculo(100, 150);
        dibujaCirculo(200, 350);
        ?>
        </svg>
        
        
        <script>
           let circulos =  document.querySelectorAll("circle");
           
           circulos.forEach(function(circulo){
               circulo.addEventListener("click", function() {
                this.style.display = "none";
            });
           });
        </script>
    </body>
</html>
