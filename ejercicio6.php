<?php
    function calculaColor() {
       $color = "rgb(" . rand(0,225) . ", " . rand(0,225) . ", " . rand(0,225) . ")"; 
       return $color;
    }

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            $color= calculaColor();
        ?>
        <p>Color: <?= $color ?></p>
        <svg width="100px" height="100px" style="display: block;margin: 0px auto;">
        <circle cx="50" cy="50" r="50" fill="<?= $color ?>">
        </svg>
    </body>
</html>
